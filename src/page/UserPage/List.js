import React, { Component } from "react";
import { connect } from "react-redux";
import { setUserAction } from "../../redux/action/user";
import axios from "axios";
import { message } from "antd";
import { SET_DATA_FORM } from "../../redux/constant/user";

class List extends Component {
  componentDidMount() {
    this.props.handleSetUser();
  }
  handleRenderTable = () => {
    return this.props.users.reverse().map((users, index) => {
      return (
        <tr key={index}>
          <td>{users.id}</td>
          <td>{users.name}</td>
          <td>{users.account}</td>
          <td>{users.password}</td>
          <td>
            <button
              onClick={() => {
                this.handleDelete(users.id);
              }}
              className="btn btn-danger mr-2"
            >
              Delete
            </button>
            <button
              onClick={() => {
                this.handleGetDetail(users.id);
              }}
              className="btn btn-info"
            >
              Edit
            </button>
          </td>
        </tr>
      );
    });
  };
  handleDelete = (id) => {
    axios
      .delete(`https://64c8b67ca1fe0128fbd61a36.mockapi.io/product/${id}`)
      .then((res) => {
        console.log(res);
        message.success("Xóa thành công");
        this.props.handleSetUser();
      })
      .catch((err) => {
        console.log(err);
      });
  };
  handleGetDetail = (id) => {
    axios
      .get(`https://64c8b67ca1fe0128fbd61a36.mockapi.io/product/${id}`)
      .then((res) => {
        console.log(res.data);
        this.props.handleSetDataForm(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  render() {
    return (
      <div>
        <table className="table">
          <thead>
            <tr>
              <th>Id</th>
              <th>Name</th>
              <th>Account</th>
              <th>Password</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>{this.handleRenderTable()}</tbody>
        </table>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    users: state.userReducer.users,
  };
};
let mapdispatchToProps = (dispatch) => {
  return {
    handleSetUser: () => {
      dispatch(setUserAction());
    },
    handleSetDataForm: (user) => {
      dispatch({ type: SET_DATA_FORM, payload: user });
    },
  };
};

export default connect(mapStateToProps, mapdispatchToProps)(List);
