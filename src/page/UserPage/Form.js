import React, { Component, createRef } from "react";
import { connect } from "react-redux";
import { setUserAction } from "../../redux/action/user";
import { SET_DATA_FORM } from "../../redux/constant/user";
import axios from "axios";
import { message } from "antd";

class Form extends Component {
  componentDidMount() {
    this.inputRef.current.focus();
  }
  inputRef = createRef();
  handleChangeForm = (event) => {
    let { value, name } = event.target;
    let user = { ...this.props.user, [name]: value };
    this.props.handleSetDataForm(user);
  };
  handleAddUser = () => {
    axios({
      url: "https://64c8b67ca1fe0128fbd61a36.mockapi.io/product",
      method: "POST",
      data: this.props.user,
    })
      .then((res) => {
        console.log(res);
        this.props.handleSetUser();
        message.success("Thêm thành công");
        this.resetForm();
      })
      .catch((err) => {
        console.log(err);
      });
  };
  handleUpdateUser = (id) => {
    axios({
      url: `https://64c8b67ca1fe0128fbd61a36.mockapi.io/product/${id}`,
      method: "PUT",
      data: this.props.user,
    })
      .then((res) => {
        console.log(res);
        this.props.handleSetUser();
        message.success("Cập nhập thành công");
        this.resetForm();
      })
      .catch((err) => {
        console.log(err);
      });
  };
  resetForm = () => {
    let user = {
      name: "",
      account: "",
      password: "",
    };
    this.props.handleSetDataForm(user);
  };
  render() {
    return (
      <div className="mt-3 mb-2">
        <form className="form-inline">
          <input
            ref={this.inputRef}
            onChange={this.handleChangeForm}
            value={this.props.user.name}
            type="text"
            class="form-control mr-2"
            name="name"
            placeholder="Name"
          />
          <input
            onChange={this.handleChangeForm}
            value={this.props.user.account}
            type="text"
            class="form-control mr-2"
            name="account"
            placeholder="Account"
          />
          <input
            onChange={this.handleChangeForm}
            value={this.props.user.password}
            type="text"
            class="form-control mr-2"
            name="password"
            placeholder="Passowrd"
          />
          <button
            onClick={this.handleAddUser}
            type="button"
            className="btn btn-dark mr-2"
          >
            Thêm
          </button>
          <button
            onClick={() => {
              this.handleUpdateUser(this.props.user.id);
            }}
            type="button"
            className="btn btn-info"
          >
            Cập nhập
          </button>
        </form>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    user: state.userReducer.user,
  };
};

let mapdispatchToProps = (dispatch) => {
  return {
    handleSetUser: () => {
      dispatch(setUserAction());
    },
    handleSetDataForm: (user) => {
      dispatch({ type: SET_DATA_FORM, payload: user });
    },
  };
};
export default connect(mapStateToProps, mapdispatchToProps)(Form);
