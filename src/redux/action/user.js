import axios from "axios";
import { SET_USER } from "../constant/user";

export let setUserAction = () => {
  return (dispatch) => {
    axios({
      url: "https://64c8b67ca1fe0128fbd61a36.mockapi.io/product",
      method: "GET",
    })
      .then((res) => {
        let action = {
          type: SET_USER,
          payload: res.data,
        };
        dispatch(action);
      })
      .catch((err) => {
        console.log(err);
      });
  };
};
