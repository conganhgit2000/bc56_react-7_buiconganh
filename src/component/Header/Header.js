import React, { Component } from "react";
import { NavLink } from "react-router-dom";

export default class Header extends Component {
  render() {
    return (
      <div>
        <NavLink className="m-4 btn btn-dark" to="/">
          Home
        </NavLink>
        <NavLink className="m-4 btn btn-dark" to="/login">
          Login
        </NavLink>
        <NavLink className="m-4 btn btn-dark" to="/life-cycle">
          Life Cycle
        </NavLink>
        <NavLink className="m-4 btn btn-dark" to="/user">
          User Page
        </NavLink>
      </div>
    );
  }
}
